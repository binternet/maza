<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CountWords
{
    use Dispatchable, SerializesModels;

    protected $q;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($q)
    {
        $this->q = $q;
    }

    /**
     * Execute the job.
     *
     */
    public function handle()
    {

        # Fix query
//        $this->q = str_replace(
//            ['"'],
//            ['"'],
//            $this->q
//        );

        $maza = ['bg19','likud'];
        $result = [];
        foreach ( $maza as $name ) {
            $result[ $name ] = substr_count( \Storage::get(sprintf('maza/%s.txt', $name) ), $this->q );
        }

        return $result;

    }
}
