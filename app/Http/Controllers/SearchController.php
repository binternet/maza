<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SearchController extends Controller
{
    function search(Request $request) {

        $q = $request->get('q');
        if ( empty( $q ) ) {
            return response()->json([
               'msg' => 'יש להזין טקסט'
            ], 400);
        }

        # Check db
        $record = \App\MazaCount::where('the_q',$q)->first();
        if ( ! empty( $record ) ) {
            return response()->json([
                'code' => 0,
                'msg' => 'OK',
                'q' => $q,
                'data' => $record->getData()
            ]);
        }

        $data = dispatch( new \App\Jobs\CountWords( $q ) );

        # Save to db
        $record = new \App\MazaCount;
        $record->the_q = $q;
        $record->bg19 = (int) $data['bg19'];
        $record->likud = (int) $data['likud'];
        $record->save();

        return response()->json([
            'code' => 0,
            'msg' => 'OK',
            'q' => $q,
            'data' => $data
        ]);

    }
}
