<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MazaCount extends Model
{
    protected $table = 'maza_count';

    function getData() {
        return [
           'bg19' => (int) $this->bg19,
           'likud' => (int) $this->likud
        ];
    }
}
