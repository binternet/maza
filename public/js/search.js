window.$resultModal = $('#modal-result');

$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form').on('submit',function(e){
        e.preventDefault();
        var query = $('[name="q"]').val();

        var jqxhr = $.post(window.gParams.base_url + '/search', { q: query }, function(response){
            updateModal(response);
            window.$resultModal.modal('show');
        })
            .fail(function(response){
                alert(response.responseJSON.msg)
            });

    });

    console.log('search.js loaded');
});

function updateModal(response) {
    console.log(response);
    window.$resultModal.find('[data-name="q"]').html(response.q);
    window.$resultModal.find('[data-name="bg19"]').html(response.data.bg19);
    window.$resultModal.find('[data-name="likud"]').html(response.data.likud);
}