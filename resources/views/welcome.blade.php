
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- COMMON TAGS -->
    <title>השוואת מצע בחירות</title>
    <!-- Search Engine -->
    <meta name="description" content="חיפוש פשוט המאפשר לבדוק כמה פעמים מילה או ביטוי מופיעים במצע הבחירות">
    <meta name="image" content="http://maza.broshi.org/assets/img/maza-preview.jpg">
    <!-- Schema.org for Google -->
    <meta itemprop="name" content="השוואת מצע בחירות">
    <meta itemprop="description" content="חיפוש פשוט המאפשר לבדוק כמה פעמים מילה או ביטוי מופיעים במצע הבחירות">
    <meta itemprop="image" content="http://maza.broshi.org/assets/img/maza-preview.jpg">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="השוואת מצע בחירות">
    <meta name="og:image" content="http://maza.broshi.org/assets/img/maza-preview.jpg">
    <meta name="og:description" content="חיפוש פשוט המאפשר לבדוק כמה פעמים מילה או ביטוי מופיעים במצע הבחירות">
    <meta name="og:type" content="website">


    <!-- Bootstrap CSS -->
    <link
            rel="stylesheet"
            href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css"
            integrity="sha384-vus3nQHTD+5mpDiZ4rkEPlnkcyTP+49BhJ4wJeJunw06ZAp+wzzeBPUXr42fi8If"
            crossorigin="anonymous">

    <style>
        body {
            background-color:lightblue;
        }
        .container {
            background-color:white;
        }
    </style>
    <script>
        window.gParams = {
            'base_url' : '{{ url('/') }}'
        }
    </script>
</head>
<body>

<div class="container">
    <div class="row">&nbsp;</div>
    <div class="row">
        <div class="col">
            <h3>חיפוש מילים וביטויים ממצע מפלגות</h3>
            <h4>הערה: החיפוש מתבצע על כל הביטוי או על חלק ממנו. כלומר, חיפוש המילה ״סמים״ יספור גם את המילה ״חסמים״</h4>
            <form method="post">
                <div class="form-group">
                    <label for="word"></label>
                    <input type="text" class="form-control" id="q" name="q" dir="rtl" aria-describedby="" placeholder="הכנס מילה או ביטוי">
                    <small id="" class="form-text text-muted"></small>
                </div>
                <button type="submit" class="btn btn-primary btn-block" style="direction: rtl">כמה פעמים זה מופיע במצע?</button>
            </form>
        </div>
    </div>
    <div class="row">&nbsp;</div>
    <div class="row">
        <div class="col">
            <table class="table table-bordered" style="direction:rtl">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">ביטוי</th>
                    <th scope="col">
                        <a href="https://bg19.co.il/wp-content/uploads/2019/03/blueandwhiteplatform.pdf" target="_blank">
                            <img src="{{ asset('assets/img/logo/kl.jpg') }}" style="width:80px" alt="">
                        </a>
                    </th>
                    <th scope="col">
                        <a href="https://www.idi.org.il/media/6698/likud-18.pdf" target="_blank">
                            <img src="{{ asset('assets/img/logo/likud.png') }}" style="width:80px" alt="">
                        </a>
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php $counter = 1; ?>
                @foreach ( \App\MazaCount::all()->sortBy('the_q') as $qCount )

                <tr>
                    <th scope="row">{{ $counter++ }}</th>
                    <td>{{ $qCount->the_q }}</td>
                    <td class="<?php echo $qCount->bg19 > $qCount->likud ? ' alert-success' : NULL ?>">{{ $qCount->bg19 }}</td>
                    <td class="<?php echo $qCount->likud > $qCount->bg19 ? ' alert-success' : NULL ?>">{{ $qCount->likud }}</td>
                </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade rtl" id="modal-result" tabindex="-1" role="dialog" aria-labelledby="modal-result" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title" id="">בדיקת מצע</h5>
            </div>
            <div class="modal-body">
                <table class="table table-bordered" style="direction:rtl">
                    <thead>
                    <tr>
                        <th scope="col">ביטוי</th>
                        <th scope="col">
                            <img src="{{ asset('assets/img/logo/kl.jpg') }}" style="width:80px" alt="">
                        </th>
                        <th scope="col">
                            <img src="{{ asset('assets/img/logo/likud.png') }}" style="width:80px" alt="">
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td data-name="q">&nbsp;</td>
                        <td data-name="bg19">&nbsp;</td>
                        <td data-name="likud">&nbsp;</td>
                    </tr>
                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">סגור</button>
            </div>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script
        src="https://cdn.rtlcss.com/bootstrap/v4.2.1/js/bootstrap.min.js"
        integrity="sha384-a9xOd0rz8w0J8zqj1qJic7GPFfyMfoiuDjC9rqXlVOcGO/dmRqzMn34gZYDTel8k"
        crossorigin="anonymous"></script>
<script src="js/search.js"></script>
</body>
</html>